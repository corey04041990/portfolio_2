const { src } = require('gulp');
const connect = require('gulp-connect')

function server(cb) {
    connect.server({
        root: './',
        livereload: true,
        port: 8888
    });

}

exports.default = server;